package org.dha.drips.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.etsy.android.grid.StaggeredGridView;

import org.dha.drips.R;
import org.dha.drips.adapters.ImageGridAdapter;
import org.dha.drips.interfaces.GridCallbacks;

public class GridFragment extends Fragment implements GridCallbacks{
	
	public static final int GRID_HOME = 0;
	public static final int GRID_COLLECTION = 1;
	public static final int GRID_STYLE = 2;
	public static final int GRID_MEDIUM = 3;
	public static final int GRID_ORIGINS = 4;
	public static final int GRID_ARTISTS = 5;
	public static final int GRID_LOCATION = 6;
	public static final int GRID_PHOTOGRAPHER = 7;
	public static final int GRID_DATE = 8;

    public static final String ARG_GRID_TYPE = "grid_type";
    public static final String ARG_ID = "id";
    public static final String ARG_QUERY = "query";
	
	private int gridType = 0;
	private int id = 0;
	private String query = "";
	
	private Context context;
	private StaggeredGridView mainGrid;
	private ImageGridAdapter homeAdapter;
	private ProgressBar loadingBar;
	
	public GridFragment(){
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.grid_fragment, null);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		context = getActivity();
		mainGrid = (StaggeredGridView)getView().findViewById(R.id.mainGrid);
		loadingBar = (ProgressBar)getView().findViewById(R.id.loadingBar);
		
		init();
	}
	
	private void init(){
		homeAdapter = new ImageGridAdapter(
                context,
                getArguments().getInt(ARG_GRID_TYPE,GRID_HOME),
                getArguments().getInt(ARG_ID,0),
                getArguments().getString(ARG_QUERY,""));
		homeAdapter.setGridCallbacks(this);
		mainGrid.setAdapter(homeAdapter);
		mainGrid.setOnScrollListener(homeAdapter);
	}

	@Override
	public void onGridLoading() {
		loadingBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onGridLoaded() {
		loadingBar.setVisibility(View.GONE);
	}

}
