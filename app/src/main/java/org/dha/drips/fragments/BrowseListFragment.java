package org.dha.drips.fragments;

import java.util.HashMap;
import java.util.Map;

import org.dha.drips.net.DripsApi;
import org.dha.drips.net.DripsApi.OnResponseListener;
import org.dha.drips.net.DripsResponse;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class BrowseListFragment extends ListFragment{
	
	private BrowseCallbacks callbacks;
	
	public static final int LIST_COLLECTIONS = 1;
	public static final int LIST_STYLES = 2;
	public static final int LIST_MEDIUMS = 3;
	public static final int LIST_ORIGINS = 4;
	public static final int LIST_ARTISTS = 5;
	public static final int LIST_LOCATION = 6;
	public static final int LIST_PHOTOGRAPHER = 7;
	public static final int LIST_DATE = 8;
	
	private int listType;
	private Map<String,Integer> hashMap;
	
	public BrowseListFragment(int listType){
		this.listType = listType;
		hashMap = new HashMap<String,Integer>();
	}
	
	private void initCollections(){
		DripsApi api = new DripsApi(getActivity());
		api.getCollections(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("name");
						hashMap.put(json.optString("name"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading collections", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void initArtists(){
		DripsApi api = new DripsApi(getActivity());
		api.getArtists(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("name");
						hashMap.put(json.optString("name"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading artists", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void initLocations(){
		DripsApi api = new DripsApi(getActivity());
		api.getLocations(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("name");
						hashMap.put(json.optString("name"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading locations", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void initMediums(){
		DripsApi api = new DripsApi(getActivity());
		api.getMediums(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("name");
						hashMap.put(json.optString("name"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading medium", Toast.LENGTH_SHORT).show();
				}
			}
		});
				
	}
	
	private void initStyles(){
		DripsApi api = new DripsApi(getActivity());
		api.getStyles(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("style");
						hashMap.put(json.optString("style"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading styles", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	private void initPhotographers(){
		DripsApi api = new DripsApi(getActivity());
		api.getPhotographers(new OnResponseListener() {
			
			@Override
			public void onResponse(DripsResponse response) {
				if(response.isSuccess()){
					JSONArray data = response.getContentArray();
					String[] entries = new String[data.length()];
					for(int i = 0; i < data.length(); ++i){
						JSONObject json = data.optJSONObject(i);
						entries[i] = json.optString("name");
						hashMap.put(json.optString("name"), json.optInt("id"));
					}
					ArrayAdapter<String> adapter = new ArrayAdapter<String>(  
						     getActivity(), android.R.layout.simple_list_item_1,  
						     entries);  
						   setListAdapter(adapter);  
				}else{
					Toast.makeText(getActivity(), "Error loading photographers", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		
		String selection = getListAdapter().getItem(position).toString();
		if(callbacks != null)
			callbacks.onBrowseItemClicked(listType, hashMap.get(selection),selection);
		
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		switch(listType){
		case LIST_COLLECTIONS:
			initCollections();
			break;
		case LIST_ARTISTS:
			initArtists();
			break;
		case LIST_MEDIUMS:
			initMediums();
			break;
		case LIST_STYLES:
			initStyles();
			break;
		case LIST_LOCATION:
			initLocations();
			break;
		case LIST_PHOTOGRAPHER:
			initPhotographers();
			break;
		}
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		getActivity().getActionBar().setTitle("Drips Gallery");
	}
	
	public void setBrowseCallbacks(BrowseCallbacks callbacks){
		this.callbacks = callbacks;
	}
	
	public interface BrowseCallbacks{
		public void onBrowseItemClicked(int gridType, int id, String selection);
	}

}
