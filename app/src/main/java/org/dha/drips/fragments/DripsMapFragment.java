package org.dha.drips.fragments;

import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

public class DripsMapFragment extends SupportMapFragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
	
	private GoogleApiClient googleApiClient;
    private Location lastLocation;
	
	public DripsMapFragment(){
		
	}

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                buildGoogleApiClient();
            }
        });

	}

    @Override
    public void onResume() {
        super.onResume();
        if(googleApiClient != null && getMap() != null){
            googleApiClient.connect();
        }
    }

    @Override
	public void onDetach() {
		super.onDetach();
		googleApiClient.disconnect();
	}
	
	private void centerMap(){
		GoogleMap map = getMap();
		CameraUpdate cu = CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude()), 10);
		map.moveCamera(cu);
	}

	@Override
	public void onConnected(Bundle arg0) {
        lastLocation = LocationServices.FusedLocationApi.getLastLocation(
                googleApiClient);
        if (lastLocation != null) {
            centerMap();
        }
	}

    @Override
    public void onConnectionSuspended(int i) {

    }

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		// TODO Auto-generated method stub
		
	}

}
