package org.dha.drips.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;

import org.dha.drips.R;
import org.dha.drips.activities.LoginActivity;
import org.dha.drips.net.DripsApi;
import org.dha.drips.net.DripsResponse;
import org.dha.drips.net.RequestManager;
import org.dha.drips.storage.LocalStore;

/**
 * Created by allan on 7/27/15.
 */
public class DrawerHeaderFragment extends Fragment {

    private ImageView headerImage;
    private TextView usernameText;
    private TransitionDrawable transitionDrawable;

    private Handler handler = new Handler();
    private Runnable imageRunnable = new Runnable() {
        @Override
        public void run() {
            if(getActivity() != null) {
                getRandomImage();
            }else{
                //wait for attachment
                handler.postDelayed(this,200);
            }
            handler.postDelayed(this, 20000);
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.drawer_header,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        headerImage = (ImageView)getView().findViewById(R.id.headerImage);
        usernameText = (TextView)getView().findViewById(R.id.usernameText);
        transitionDrawable = new TransitionDrawable(new Drawable[] {
                new ColorDrawable(Color.TRANSPARENT),
                new ColorDrawable(Color.TRANSPARENT)
        });

        init();
    }

    private void init(){
        transitionDrawable.setCrossFadeEnabled(true);
        headerImage.setImageDrawable(transitionDrawable);
        transitionDrawable.startTransition(500);
        configureImageClickBehavior();
    }

    private void configureImageClickBehavior(){
        LocalStore store = new LocalStore(getActivity());
        if(store.isSet()){
            setUpUserHeader(store);
        }else{
            setUpLoginHeader();
        }
    }

    private void setUpUserHeader(LocalStore store){
        usernameText.setText(store.getUsername());
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO implement user profile
            }
        });
    }

    private void setUpLoginHeader(){
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLoginActivity();
            }
        });
    }

    private void goToLoginActivity(){
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() != null) {
            handler.post(imageRunnable);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        handler.removeCallbacks(imageRunnable);
    }

    private void getRandomImage(){
        DripsApi api = new DripsApi(getActivity());
        api.getRandomImage(new DripsApi.OnResponseListener() {
            @Override
            public void onResponse(DripsResponse response) {
                if(response.isSuccess()){
                    String url = response.getString("image_url");

                    ImageRequest request = new ImageRequest(url,
                            new Response.Listener<Bitmap>() {
                                @Override
                                public void onResponse(Bitmap bitmap) {
                                    transitionDrawable.setDrawableByLayerId(transitionDrawable.getId(1),new BitmapDrawable(getActivity().getResources(),bitmap));
                                    transitionDrawable.resetTransition();
                                    transitionDrawable.setCrossFadeEnabled(true);
                                }
                            }, 0, 0, null,
                            new Response.ErrorListener() {
                                public void onErrorResponse(VolleyError error) {
                                    headerImage.setImageDrawable(null);
                                }
                            });

                    RequestManager.getInstance(getActivity()).addToRequestQueue(request);
                }
            }
        });
    }
}
