package org.dha.drips.entries;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by allan on 7/31/15.
 */
public class ImageEntry {

    private Context context;
    private JSONObject json;

    public ImageEntry(Context context, String title, String collection) throws JSONException {
        this.json = new JSONObject();
        this.json.put("title",title);
        this.json.put("collection",collection);
        this.context = context;
    }

    public void setDateTaken(String dateTaken) throws JSONException {
        this.json.put("date_taken",dateTaken);
    }

    public void setLcTgm(String tgm) throws JSONException{
        this.json.put("tgm",tgm);
    }

    public void setStyleName(String styleName) throws JSONException{
        this.json.put("style",styleName);
    }

    public void setMediumName(String mediumName) throws JSONException{
        this.json.put("medium",mediumName);
    }

    public void setArtistName(String artistName) throws JSONException{
        this.json.put("artist",artistName);
    }

    public void setPhotographer(String photographer) throws JSONException{
        this.json.put("photographer",photographer);
    }

    public void setLocation(Location location) throws JSONException{
        this.json.put("latitude",location.getLatitude());
        this.json.put("longitude",location.getLongitude());
        reverseGeocodeCity(location);
    }

    private void reverseGeocodeCity(Location location){
        new AsyncTask<Location,Void,Address>(){

            @Override
            protected Address doInBackground(Location... params) {
                double lat = params[0].getLatitude();
                double lng = params[0].getLongitude();
                Address address = null;
                if(Geocoder.isPresent()){
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    try {
                        List<Address> addresses = geocoder.getFromLocation(lat,lng,1);
                        address = addresses.size() > 0 ? addresses.get(0) : null;
                    } catch (IOException e) {
                        address = null;
                    }
                }
                return address;
            }

            @Override
            protected void onPostExecute(Address address) {
                super.onPostExecute(address);
                if(address != null){
                    String city="";
                    if (address.getLocality() != null)  city=address.getLocality();
                    else
                    if (address.getSubAdminArea() != null)  city=address.getSubAdminArea();

                    try {
                        ImageEntry.this.json.put("location",city);
                    } catch (JSONException e) {
                        return;
                    }
                }
            }
        }.execute(location);
    }

    @Override
    public String toString() {
        return this.json.toString();
    }
}
