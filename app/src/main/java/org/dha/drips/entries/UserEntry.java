package org.dha.drips.entries;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by allan on 7/31/15.
 */
public class UserEntry {

    private Context context;
    private JSONObject json;

    public UserEntry(String username, String email, String password) throws JSONException{
        this.json.put("username",username);
        this.json.put("email",email);
        this.json.put("password",password);
    }
}
