package org.dha.drips.net;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class DripsResponse {
	
	private JSONObject json;
	
	public DripsResponse(String response) throws JSONException{
		this.json = new JSONObject(response);
	}
	
	public boolean isSuccess(){
		return json.optBoolean("execution");
	}
	
	public JSONArray getContentArray(){
		return json.optJSONArray("data");
	}
	
	public JSONObject getFirstObject(){
		return getContentArray().optJSONObject(0);
	}

    public String getString(String key){
        return json.optString(key);
    }

}
