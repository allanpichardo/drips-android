package org.dha.drips.net.responseentities;

import org.json.JSONException;
import org.json.JSONObject;

public class DripsImage {
	
	private JSONObject json;
	
	public DripsImage(JSONObject json){
		this.json = json;
	}
	
	public DripsImage(String json) throws JSONException{
		this.json = new JSONObject(json);
	}
	
	public int getId(){
		return json.optInt("image_id");
	}
	
	public String getDateTaken(){
		return json.optString("date_taken");
	}
	
	public String getFileName(){
		return json.optString("file_name");
	}
	
	public String getTitle(){
		return json.optString("title");
	}
	
	public int getCollectionId(){
		return json.optInt("collection_id");
	}
	
	public String getCollectionName(){
		return json.optString("collection_name");
	}
	
	public String getStyles(){
		return json.optString("styles");
	}
	
	public String getEquipment(){
		return json.optString("equipment");
	}
	
	public String getOrigin(){
		return json.optString("origin");
	}
	
	public String getArtistNames(){
		return json.optString("artist_names");
	}

    public String getArtistIds(){
        return json.optString("artist_ids");
    }
	
	public String getLocationName(){
		return json.optString("location_name");
	}
	
	public String getPhotographerName(){
		return json.optString("photographer_name");
	}
	
	public String getLcTgm(){
		return json.optString("lc_tgm");
	}
	
	public String getUsername(){
		return json.optString("user_name");
	}
	
	public int getUserId(){
		return json.optInt("user_id");
	}
	
	public String getImageUrl(){
		return json.optString("image_url");
	}
	
	public String getThumbnailUrl(){
		return json.optString("thumbnail_url");
	}
	
	@Override
	public String toString() {
		return json.toString();
	}

}
