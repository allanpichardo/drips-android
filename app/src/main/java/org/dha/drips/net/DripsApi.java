package org.dha.drips.net;

import android.content.Context;

import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class DripsApi {
	
	RequestManager requestManager;
	
	public DripsApi(Context context){
		requestManager = RequestManager.getInstance(context.getApplicationContext());
	}
	
	public void getCollectionImages(int page, int id, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("collection", String.valueOf(id));
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getArtistImages(int page, String query, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("artist_query", query);
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getStyleImages(int page, int id, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("style", String.valueOf(id));
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getLocationImages(int page, String query, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("location_query", query);
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getMediumImages(int page, int id, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("medium", String.valueOf(id));
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getPhotographerImages(int page, String query, final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		params.put("photographer_query", query);
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getImages(int page,final OnResponseListener listener){
		if(page == 0){
			page = 1;
		}
		String p = String.valueOf(page);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_browse_images");
		params.put("page",p);
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getImage(int id, final OnResponseListener listener){
		String imageId = String.valueOf(id);
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_image");
		params.put("image_id",imageId);
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getCollections(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_collections");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getArtists(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_artists");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getStyles(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_styles");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getLocations(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_locations");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getMediums(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_mediums");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getPhotographers(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_photographers");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}
	
	public void getOrigins(final OnResponseListener listener){
		Map<String,String> params = new HashMap<String,String>();
		params.put("fn", "get_origins");
		
		DripsRequest request = new DripsRequest(params, new Listener<String>(){

			@Override
			public void onResponse(String response) {
				try {
					DripsResponse res = new DripsResponse(response);
					listener.onResponse(res);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}, new ErrorListener(){

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub
				
			}
			
		});
		requestManager.addToRequestQueue(request);
	}

    public void getRandomImage(final OnResponseListener listener){
        Map<String,String> params = new HashMap<String,String>();
        params.put("fn", "random_image");

        DripsRequest request = new DripsRequest(params, new Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    DripsResponse res = new DripsResponse(response);
                    listener.onResponse(res);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }, new ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }

        });
        requestManager.addToRequestQueue(request);
    }

    public void checkUsernameAvailable(String username, final OnResponseListener listener){
        Map<String,String> params = new HashMap<String,String>();
        params.put("fn", "is_username_available");
        params.put("username",username);

        DripsRequest request = new DripsRequest(params, new Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    DripsResponse res = new DripsResponse(response);
                    listener.onResponse(res);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }, new ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }

        });
        requestManager.addToRequestQueue(request);
    }

    public void checkEmailAvailable(String email, final OnResponseListener listener){
        Map<String,String> params = new HashMap<String,String>();
        params.put("fn", "is_email_available");
        params.put("email",email);

        DripsRequest request = new DripsRequest(params, new Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    DripsResponse res = new DripsResponse(response);
                    listener.onResponse(res);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }, new ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }

        });
        requestManager.addToRequestQueue(request);
    }

    public void authenticate(String email, String password, final OnResponseListener listener){
        Map<String,String> params = new HashMap<String,String>();
        params.put("fn", "authenticate");
        params.put("email",email);
        params.put("password",password);

        DripsRequest request = new DripsRequest(params, new Listener<String>(){

            @Override
            public void onResponse(String response) {
                try {
                    DripsResponse res = new DripsResponse(response);
                    listener.onResponse(res);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }, new ErrorListener(){

            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO Auto-generated method stub

            }

        });
        requestManager.addToRequestQueue(request);
    }
	
	public interface OnResponseListener{
		void onResponse(DripsResponse response);
	}

}
