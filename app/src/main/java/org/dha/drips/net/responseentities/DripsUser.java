package org.dha.drips.net.responseentities;

import org.json.JSONObject;

import java.sql.Date;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by allan on 7/31/15.
 */
public class DripsUser {

    private JSONObject json;

    public DripsUser(JSONObject json){
        this.json = json;
    }

    public int getId(){
        return json.optInt("id");
    }

    public String getUsername(){
        return json.optString("username");
    }

    public String getEmail(){
        return json.optString("email");
    }

    public String getImageUrl(){
        return json.optString("image_url");
    }

    public Calendar getCreatedCalendar(){
        Date date = Date.valueOf(json.optString("created"));
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault(), Locale.getDefault());
        calendar.setTime(date);
        return calendar;
    }

    public boolean isAdmin(){
        return json.optInt("is_admin") == 1;
    }
}
