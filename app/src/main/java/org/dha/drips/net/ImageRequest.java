package org.dha.drips.net;

import com.android.volley.Response;

import java.io.File;

/**
 * Created by allan on 7/31/15.
 */
public class ImageRequest extends MultipartRequest {

    public ImageRequest(String url, Response.ErrorListener errorListener, Response.Listener<String> listener, File file, String stringPart) {
        super(url, errorListener, listener, file, stringPart);
    }

}
