package org.dha.drips.net;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dha.drips.constants.Constants;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.RetryPolicy;
import com.android.volley.toolbox.StringRequest;



public class DripsRequest extends StringRequest{

	private Map<String,String> parameters;
	private RetryPolicy policy;

	public DripsRequest(Map<String,String> parameters, Listener<String> listener,
			ErrorListener errorListener) {
		super(Method.POST, Constants.DRIPS_URL, listener, errorListener);
		this.parameters = parameters;
		setTimeout(0);
		policy = new DefaultRetryPolicy(1000 * 30, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
		
		printDebug();
	}
	
	public String getFullUrl(){
		List<String> keys = new ArrayList<String>(parameters.keySet());
		List<String> values = new ArrayList<String>(parameters.values());
		String url = Constants.DRIPS_URL + "?";
		
		for(int i = 0; i < parameters.keySet().size(); ++i){
			url += keys.get(i) + "=" + values.get(i);
			if(i != parameters.keySet().size() -1){
				url += "&";
			}
		}
		return url;
	}
	
	private void printDebug(){
		
		
		Log.d(parameters.get("fn"), getFullUrl());
	}

	@Override
	protected Map<String, String> getParams() throws AuthFailureError {
		return this.parameters;
	}
	
	@Override
	public com.android.volley.Request.Priority getPriority() {
		return Priority.HIGH;
	}

	@Override
	public String getCacheKey() {

		return getFullUrl();
	}

	@Override
	public RetryPolicy getRetryPolicy() {
		return policy;
	}

	public void setTimeout(int seconds){
		policy = new DefaultRetryPolicy(1000 * seconds, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
		setRetryPolicy(policy);
	}
	

}
