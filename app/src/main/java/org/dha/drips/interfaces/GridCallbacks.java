package org.dha.drips.interfaces;

public interface GridCallbacks {
	
	public void onGridLoading();
	public void onGridLoaded();

}
