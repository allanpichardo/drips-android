package org.dha.drips.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.dha.drips.net.DripsApi;
import org.dha.drips.net.responseentities.DripsImage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by allan on 7/31/15.
 */
public class ImageORM {

    private static final String TAG = "ImageORM";

    private static final String TABLE_NAME = "image";

    private static final String COMMA_SEP = ", ";

    private static final String COLUMN_ID_TYPE = "INTEGER PRIMARY KEY";
    private static final String COLUMN_ID = "id";

    private static final String COLUMN_DATE_TYPE = "TEXT";
    private static final String COLUMN_DATE = "date_taken";

    private static final String COLUMN_FILENAME_TYPE = "TEXT";
    private static final String COLUMN_FILENAME = "file_name";

    private static final String COLUMN_TITLE_TYPE = "TEXT";
    private static final String COLUMN_TITLE = "title";

    private static final String COLUMN_COLLECTION_ID_TYPE = "INTEGER";
    private static final String COLUMN_COLLECTION_ID = "collection_id";

    private static final String COLUMN_COLLECTION_NAME_TYPE = "TEXT";
    private static final String COLUMN_COLLECTION_NAME = "collection_name";

    private static final String COLUMN_STYLES_TYPE = "TEXT";
    private static final String COLUMN_STYLES = "styles";

    private static final String COLUMN_EQUIPMENT_TYPE = "TEXT";
    private static final String COLUMN_EQUIPMENT = "equipment";

    private static final String COLUMN_ORIGIN_TYPE = "TEXT";
    private static final String COLUMN_ORIGIN = "origin";

    private static final String COLUMN_ARTIST_NAMES_TYPE = "TEXT";
    private static final String COLUMN_ARTIST_NAMES = "artist_names";

    private static final String COLUMN_ARTIST_IDS_TYPE = "TEXT";
    private static final String COLUMN_ARTIST_IDS = "artist_ids";

    private static final String COLUMN_LOCATION_NAME_TYPE = "TEXT";
    private static final String COLUMN_LOCATION_NAME = "location_name";

    private static final String COLUMN_PHOTOGRAPHER_NAME_TYPE = "TEXT";
    private static final String COLUMN_PHOTOGRAPHER_NAME = "photographer_name";

    private static final String COLUMN_LC_TGM_TYPE = "TEXT";
    private static final String COLUMN_LC_TGM = "lc_tgm";

    private static final String COLUMN_USERNAME_TYPE = "TEXT";
    private static final String COLUMN_USERNAME = "username";

    private static final String COLUMN_USER_ID_TYPE = "INTEGER";
    private static final String COLUMN_USER_ID = "user_id";

    private static final String COLUMN_IMAGE_URL_TYPE = "TEXT";
    private static final String COLUMN_IMAGE_URL = "image_url";

    private static final String COLUMN_THUMBNAIL_URL_TYPE = "TEXT";
    private static final String COLUMN_THUMBNAIL_URL = "thumbnail_url";

    public static final String SQL_CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLUMN_ID + " " + COLUMN_ID_TYPE + COMMA_SEP +
                    COLUMN_DATE + " " + COLUMN_DATE_TYPE + COMMA_SEP +
                    COLUMN_FILENAME + " " + COLUMN_FILENAME_TYPE + COMMA_SEP +
                    COLUMN_TITLE  + " " + COLUMN_TITLE_TYPE + COMMA_SEP +
                    COLUMN_COLLECTION_ID  + " " + COLUMN_COLLECTION_ID_TYPE + COMMA_SEP +
                    COLUMN_COLLECTION_NAME  + " " + COLUMN_COLLECTION_NAME_TYPE + COMMA_SEP +
                    COLUMN_STYLES  + " " + COLUMN_STYLES_TYPE + COMMA_SEP +
                    COLUMN_EQUIPMENT  + " " + COLUMN_EQUIPMENT_TYPE + COMMA_SEP +
                    COLUMN_ORIGIN  + " " + COLUMN_ORIGIN_TYPE + COMMA_SEP +
                    COLUMN_ARTIST_NAMES  + " " + COLUMN_ARTIST_NAMES_TYPE + COMMA_SEP +
                    COLUMN_ARTIST_IDS  + " " + COLUMN_ARTIST_IDS_TYPE + COMMA_SEP +
                    COLUMN_LOCATION_NAME  + " " + COLUMN_LOCATION_NAME_TYPE + COMMA_SEP +
                    COLUMN_PHOTOGRAPHER_NAME  + " " + COLUMN_PHOTOGRAPHER_NAME_TYPE + COMMA_SEP +
                    COLUMN_LC_TGM  + " " + COLUMN_LC_TGM_TYPE + COMMA_SEP +
                    COLUMN_USERNAME  + " " + COLUMN_USERNAME_TYPE + COMMA_SEP +
                    COLUMN_USER_ID  + " " + COLUMN_USER_ID_TYPE + COMMA_SEP +
                    COLUMN_IMAGE_URL  + " " + COLUMN_IMAGE_URL_TYPE + COMMA_SEP +
                    COLUMN_THUMBNAIL_URL  + " " + COLUMN_THUMBNAIL_URL_TYPE +
                    ")";

    public static final String SQL_DROP_TABLE =
            "DROP TABLE IF EXISTS " + TABLE_NAME;

    public static void insertImage(Context context, DripsImage image){
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getWritableDatabase();

        ContentValues values = imageToContentValues(image);
        long imageId = database.insertWithOnConflict(ImageORM.TABLE_NAME, "null", values, SQLiteDatabase.CONFLICT_IGNORE);
        Log.i(TAG, "Inserted new Image with ID: " + imageId);

        database.close();
    }

    private static ContentValues imageToContentValues(DripsImage image){
        ContentValues values = new ContentValues();
        values.put(ImageORM.COLUMN_ID, image.getId());
        values.put(ImageORM.COLUMN_DATE,image.getDateTaken());
        values.put(ImageORM.COLUMN_FILENAME,image.getFileName());
        values.put(ImageORM.COLUMN_TITLE,image.getTitle());
        values.put(ImageORM.COLUMN_COLLECTION_ID,image.getCollectionId());
        values.put(ImageORM.COLUMN_COLLECTION_NAME,image.getCollectionName());
        values.put(ImageORM.COLUMN_STYLES,image.getStyles());
        values.put(ImageORM.COLUMN_EQUIPMENT,image.getEquipment());
        values.put(ImageORM.COLUMN_ORIGIN,image.getOrigin());
        values.put(ImageORM.COLUMN_ARTIST_NAMES,image.getArtistNames());
        values.put(ImageORM.COLUMN_ARTIST_IDS,image.getArtistIds());
        values.put(ImageORM.COLUMN_LOCATION_NAME,image.getLocationName());
        values.put(ImageORM.COLUMN_PHOTOGRAPHER_NAME,image.getPhotographerName());
        values.put(ImageORM.COLUMN_LC_TGM,image.getLcTgm());
        values.put(ImageORM.COLUMN_USERNAME,image.getUsername());
        values.put(ImageORM.COLUMN_USER_ID,image.getUserId());
        values.put(ImageORM.COLUMN_IMAGE_URL,image.getImageUrl());
        values.put(ImageORM.COLUMN_THUMBNAIL_URL,image.getThumbnailUrl());
        return values;
    }

    public static List<DripsImage> getImages(Context context, int page){
        DatabaseWrapper databaseWrapper = new DatabaseWrapper(context);
        SQLiteDatabase database = databaseWrapper.getReadableDatabase();

        Cursor cursor = database.rawQuery("SELECT * FROM " + ImageORM.TABLE_NAME, null);

        Log.i(TAG, "Loaded " + cursor.getCount() + " Images...");
        List<DripsImage> imageList = new ArrayList<DripsImage>();

        if(cursor.getCount() > 0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                DripsImage image = cursorToImage(cursor);
                imageList.add(image);
                cursor.moveToNext();
            }
            Log.i(TAG, "Images loaded successfully.");
        }

        database.close();

        return imageList;
    }

    public static void synchronizeImages(Context context, int page, DripsApi.OnResponseListener listener){
        DripsApi api = new DripsApi(context);
        api.getImages(page, listener);
    }

    private static DripsImage cursorToImage(Cursor cursor){
        JSONObject json = new JSONObject();
        try {
            json.put("image_id",cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
            json.put("date_taken",cursor.getInt(cursor.getColumnIndex(COLUMN_DATE)));
            json.put("file_name",cursor.getInt(cursor.getColumnIndex(COLUMN_FILENAME)));
            json.put("title",cursor.getInt(cursor.getColumnIndex(COLUMN_TITLE)));
            json.put("collection_id",cursor.getInt(cursor.getColumnIndex(COLUMN_COLLECTION_ID)));
            json.put("collection_name",cursor.getInt(cursor.getColumnIndex(COLUMN_COLLECTION_NAME)));
            json.put("styles",cursor.getInt(cursor.getColumnIndex(COLUMN_STYLES)));
            json.put("equipment",cursor.getInt(cursor.getColumnIndex(COLUMN_EQUIPMENT)));
            json.put("origin",cursor.getInt(cursor.getColumnIndex(COLUMN_ORIGIN)));
            json.put("artist_names",cursor.getInt(cursor.getColumnIndex(COLUMN_ARTIST_NAMES)));
            json.put("artist_ids",cursor.getInt(cursor.getColumnIndex(COLUMN_ARTIST_IDS)));
            json.put("location_name",cursor.getInt(cursor.getColumnIndex(COLUMN_LOCATION_NAME)));
            json.put("photographer_name",cursor.getInt(cursor.getColumnIndex(COLUMN_PHOTOGRAPHER_NAME)));
            json.put("lc_tgm",cursor.getInt(cursor.getColumnIndex(COLUMN_LC_TGM)));
            json.put("username",cursor.getInt(cursor.getColumnIndex(COLUMN_USERNAME)));
            json.put("user_id",cursor.getInt(cursor.getColumnIndex(COLUMN_USER_ID)));
            json.put("image_url",cursor.getInt(cursor.getColumnIndex(COLUMN_IMAGE_URL)));
            json.put("thumbnail_url",cursor.getInt(cursor.getColumnIndex(COLUMN_THUMBNAIL_URL)));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new DripsImage(json);
    }
}
