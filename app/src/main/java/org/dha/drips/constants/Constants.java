package org.dha.drips.constants;

public class Constants {
	
	//public static String DRIPS_URL = "http://www.dhalab.org/dripsgallery/api/drips.php";
    public static String DRIPS_URL = "http://98.14.121.164/drips-gallery/api/drips2.php";
	
	public static String FUNCTION_RANDOM_IMAGE = "random_image";
	public static String FUNCTION_GET_COLLECTIONS = "get_collections";
	public static String FUNCTION_GET_STYLES = "get_styles";
	public static String FUNCTION_GET_MEDIUMS = "get_mediums";
	public static String FUNCTION_GET_DIGITAL_SPECS = "get_digital_specs";
	public static String FUNCTION_GET_BROWSE_IMAGES = "get_browse_images";
	public static String FUNCTION_GET_IMAGE = "get_image";

}
