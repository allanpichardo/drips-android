package org.dha.drips;



import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

import org.dha.drips.fragments.BrowseListFragment;
import org.dha.drips.fragments.BrowseListFragment.BrowseCallbacks;
import org.dha.drips.fragments.GridFragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity implements
NavigationDrawerFragment.NavigationDrawerCallbacks, BrowseCallbacks {

	public static final int REQUEST_IMAGE = 101;

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private GridFragment gridFragment;

	private Uri outputFileUri;

	private ImageButton homeButton;
	private ImageButton cameraButton;
	private ImageButton mapButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);

		gridFragment = new GridFragment();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		homeButton = (ImageButton)findViewById(R.id.homeButton);
		cameraButton = (ImageButton)findViewById(R.id.cameraButton);
		mapButton = (ImageButton)findViewById(R.id.mapButton);

		init();
	}

	private void init(){
		ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle("Drips Gallery");
		actionBar.setIcon(android.R.drawable.ic_menu_search);

		attachHomeGrid();

		homeButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				attachHomeGrid();
			}
		});
		cameraButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				openImageIntent();
			}
		});
		mapButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				attachMap();
			}
		});

	}

	private void openImageIntent() {

		// Determine Uri of camera image to save.
		final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "drips" + File.separator);
		root.mkdirs();
		final String fname = System.currentTimeMillis() + ".jpg";
		final File sdImageMainDirectory = new File(root, fname);
		outputFileUri = Uri.fromFile(sdImageMainDirectory);

		// Camera.
		final List<Intent> cameraIntents = new ArrayList<Intent>();
		final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		final PackageManager packageManager = getPackageManager();
		final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
		for(ResolveInfo res : listCam) {
			final String packageName = res.activityInfo.packageName;
			final Intent intent = new Intent(captureIntent);
			intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
			intent.setPackage(packageName);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
			cameraIntents.add(intent);
		}

		// Filesystem.
		final Intent galleryIntent = new Intent();
		galleryIntent.setType("image/*");
		galleryIntent.setAction(Intent.ACTION_GET_CONTENT);

		// Chooser of filesystem options.
		final Intent chooserIntent = Intent.createChooser(galleryIntent, "Select Source");

		// Add the camera options.
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[]{}));

		startActivityForResult(chooserIntent, REQUEST_IMAGE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(resultCode == RESULT_OK)
		{
			if(requestCode == REQUEST_IMAGE)
			{
				final boolean isCamera;
				if(data == null)
				{
					isCamera = true;
				}
				else
				{
					final String action = data.getAction();
					if(action == null)
					{
						isCamera = false;
					}
					else
					{
						isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
					}
				}

				Uri selectedImageUri;
				if(isCamera)
				{
					selectedImageUri = outputFileUri;
				}
				else
				{
					selectedImageUri = data == null ? null : data.getData();
				}
			}
		}
	}

	private void attachHomeGrid(){
		//attach the main grid
		/*FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, gridFragment, "maingrid");
		transaction.commit();

		getActionBar().setTitle("Drips Gallery");*/
	}

	private void attachMap(){
		//attach the map
		/*DripsMapFragment dripsMap = new DripsMapFragment();
		
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, dripsMap, "dripsmap");
		transaction.commit();

		getActionBar().setTitle("Drips Gallery");*/
	}

	public void restoreActionBar() {

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			/*getMenuInflater().inflate(R.menu.main, menu);
			restoreActionBar();*/
			return false;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSearch(String query) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCollectionsClicked() {
		//attach the collection list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_COLLECTIONS);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "collectionlist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Collection");
	}

	@Override
	public void onArtistsClicked() {
		//attach the artist list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_ARTISTS);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "artistlist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Artist");
	}

	@Override
	public void onStyleClicked() {
		//attach the style list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_STYLES);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "stylelist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Styles");
	}

	@Override
	public void onLocationClicked() {
		//attach the location list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_LOCATION);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "locationlist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Location");
	}

	@Override
	public void onMediumClicked() {
		//attach the medium list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_MEDIUMS);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "mediumlist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Medium");
	}

	@Override
	public void onPhotographerClicked() {
		//attach the photographer list
		BrowseListFragment listFragment = new BrowseListFragment(BrowseListFragment.LIST_PHOTOGRAPHER);
		listFragment.setBrowseCallbacks(this);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, listFragment, "photographerlist");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle("Photographer");
	}

	@Override
	public void onDateClicked() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onOriginClicked() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onBrowseItemClicked(int gridType, int id, String selection) {
		//attach the medium list
		/*GridFragment gridFragment = new GridFragment(gridType,id,selection);

		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.replace(R.id.container, gridFragment, "userselection");
		transaction.addToBackStack(null);
		transaction.commit();

		getActionBar().setTitle(selection);*/
	}

}
