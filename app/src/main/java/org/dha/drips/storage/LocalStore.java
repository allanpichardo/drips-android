package org.dha.drips.storage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by allan on 7/31/15.
 */
public class LocalStore {

    public static final String PREFS = "prefs";

    private SharedPreferences sharedPreferences;

    public LocalStore(Context context){
        this.sharedPreferences = context.getSharedPreferences(PREFS,0);
    }

    public void setUsername(String username){
        sharedPreferences.edit().putString("username",username).commit();
    }

    public String getUsername(){
        return sharedPreferences.getString("username","");
    }

    public void setEmail(String email){
        sharedPreferences.edit().putString("email",email).commit();
    }

    public String getEmail(){
        return sharedPreferences.getString("email","");
    }

    public void setUserId(int id){
        sharedPreferences.edit().putInt("user_id",id).commit();
    }

    public int getUserId(){
        return sharedPreferences.getInt("user_id",0);
    }

    public boolean isSet(){
        return !getUsername().isEmpty() && !getEmail().isEmpty() && getUserId() != 0;
    }
}
