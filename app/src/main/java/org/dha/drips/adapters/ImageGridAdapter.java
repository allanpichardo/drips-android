package org.dha.drips.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.android.volley.toolbox.ImageLoader.ImageListener;

import org.dha.drips.R;
import org.dha.drips.fragments.GridFragment;
import org.dha.drips.interfaces.GridCallbacks;
import org.dha.drips.net.DripsApi;
import org.dha.drips.net.DripsApi.OnResponseListener;
import org.dha.drips.net.responseentities.DripsImage;
import org.dha.drips.net.DripsResponse;
import org.dha.drips.net.RequestManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ImageGridAdapter extends BaseAdapter implements OnScrollListener{

	private GridCallbacks callbacks;

	private Context context;
	private List<DripsImage> images;

	private boolean loadingMore = false;
	private int page = 1;
	private int gridType = 0;
	private int id = 0;
	private String query = "";

	public ImageGridAdapter(Context context){
		this.context = context;
		images = new ArrayList<DripsImage>();
		refresh();
	}
	
	public ImageGridAdapter(Context context, int gridType, int id, String query){
		this.gridType = gridType;
		this.id = id;
		this.context = context;
		this.query = query;
		images = new ArrayList<DripsImage>();
		refresh();
	}
	
	public void refreshHome(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getImages(page, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshCollections(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getCollectionImages(page, id, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshArtists(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getArtistImages(page, query, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshStyles(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getStyleImages(page, id, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshLocations(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getLocationImages(page, query, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshMedium(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getMediumImages(page, id, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess()){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}
	
	public void refreshPhotographers(){
		if(callbacks != null)
			callbacks.onGridLoading();

		DripsApi api = new DripsApi(context);
		loadingMore = true;
		api.getPhotographerImages(page, query, new OnResponseListener() {

			@Override
			public void onResponse(DripsResponse response) {
				loadingMore = false;
				if(response.isSuccess() && response.getContentArray() != null){
					appendImages(response.getContentArray());
				}else{
					//Toast.makeText(context, "Error loading images", Toast.LENGTH_SHORT).show();
				}
				if(callbacks != null)
					callbacks.onGridLoaded();
			}
		});
	}

	public void setGridCallbacks(GridCallbacks gridCallbacks){
		this.callbacks = gridCallbacks;
	}

	public void refresh(){
		switch(gridType){
		case GridFragment.GRID_COLLECTION:
			refreshCollections();
			break;
		case GridFragment.GRID_HOME:
			refreshHome();
			break;
		case GridFragment.GRID_MEDIUM:
			refreshMedium();
			break;
		case GridFragment.GRID_STYLE:
			refreshStyles();
			break;
		case GridFragment.GRID_ARTISTS:
			refreshArtists();
			break;
		case GridFragment.GRID_LOCATION:
			refreshLocations();
			break;
		case GridFragment.GRID_PHOTOGRAPHER:
			refreshPhotographers();
			break;
		}
	}

	private void appendImages(JSONArray array){
		for(int i = 0; i < array.length(); ++i){
			JSONObject j = array.optJSONObject(i);
			DripsImage image = new DripsImage(j);
			images.add(image);
		}
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return images.size();
	}

	@Override
	public Object getItem(int position) {
		return images.get(position);
	}

	@Override
	public long getItemId(int position) {
		return images.get(position).getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.image_grid_item, null);
		}

		DripsImage image = (DripsImage)getItem(position);
		final ImageView imageView = (ImageView)convertView.findViewById(R.id.image);
		//imageView.setImageDrawable(null);
		ImageLoader imageLoader = RequestManager.getInstance(context.getApplicationContext()).getImageLoader();
		imageLoader.get(image.getThumbnailUrl(), new ImageListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onResponse(ImageContainer response, boolean isImmediate) {
				if(response != null && response.getBitmap() != null){
					imageView.setImageBitmap(response.getBitmap());
				}
			}
		});

		return convertView;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		//what is the bottom iten that is visible
		int lastInScreen = firstVisibleItem + visibleItemCount;

		//is the bottom item visible & not loading more already ? Load more !
		if((lastInScreen == totalItemCount) && (loadingMore)){
			notifyDataSetChanged(); 
		}
		if((lastInScreen == totalItemCount) && !(loadingMore)){
			page++;
			refresh();
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// TODO Auto-generated method stub

	}

}
